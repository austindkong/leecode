package austin.file;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Description:
 * @Author: Austin
 * @Create: 12/5/2023 9:27 PM
 */
@Slf4j
public class MoveFilesByCreateDate {

    private static String ROOT_FOLDER = "D:\\photo_mobilephone\\2023";

    public static DateTimeFormatter datetimeFormat = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");
    public static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static DateTimeFormatter yearMonthFormat = DateTimeFormatter.ofPattern("yyyy-MM");


    public static void main(String[] args) {

        try {
            Path root = Paths.get(ROOT_FOLDER);
            List<Path> paths = listFiles(root);
            int count = 0;
            for (Path path : paths) {
                if (getSuffix(path.toString().toLowerCase(Locale.ROOT)).equals("jpg")) {

                    String fname = getFilenameFromPath(path.toString());
                    log.info("{}-> {}", count, path.toString());
                    Metadata metadata = ImageMetadataReader.readMetadata(path.toFile());

                    for (Directory directory : metadata.getDirectories()) {
                        String model = "";
                        String strDateTime = "";
                        for (Tag tag : directory.getTags()) {
                            if("Model".equals(tag.getTagName())){
                                model = tag.getDescription();
                            }
                            if("Date/Time".equals(tag.getTagName())){
                                strDateTime = tag.getDescription();
                            }
                        }
                        if(model.length() > 0 && strDateTime.length() > 0){
                            count++;
                            LocalDate date = LocalDate.parse(strDateTime, datetimeFormat);

                            Path newPath = getPath(date.format(yearMonthFormat), fname);
                            if(!path.toString().equals(newPath.toString()))
                            {
                                Files.move(path, newPath);
                                log.info("{} has been moved to {}", path.toString(), newPath.toString());
                            }

                        }
                    }


                   /* if (!Objects.isNull(fname)) {
                        Files.move(path, path.resolveSibling(fname + ".done"));
                    }*/
                } else {
                    //log.warn("ignore austin.file {} because of the incorrect suffix", path.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getSuffix(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static String getFilenameFromPath(String fileName) {
        int pos = fileName.lastIndexOf(FileSystems.getDefault().getSeparator());
        return pos > 0 ? fileName.substring(pos + 1) : null;
    }

    public static List<Path> listFiles(Path path) throws IOException {
        List<Path> result;
        try (Stream<Path> walk = Files.walk(path)) {
            result = walk.filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        }
        return result;
    }

    public static Path getPath(String... more) throws IOException {
        Path result = Paths.get(ROOT_FOLDER, more);
        Files.createDirectories(result.getParent());
        return result;
    }



}
