package austin.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @Description:
 * @Author: Austin
 * @Create: 1/18/2024 10:42 PM
 */
@Slf4j
public class ThreadTest {

    static List<Integer> numberList = new ArrayList<>();

    volatile Integer count;

    static synchronized  boolean isGoodGuest(Integer number){
        Iterator<Integer> iterator = numberList.iterator();

        while(iterator.hasNext()){
            Integer value = iterator.next();
            if(number == value){
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {

        numberList.add(23);
        numberList.add(85);
        numberList.add(12);
        numberList.add(33);
        numberList.add(65);

        Optional<Integer> max = numberList.stream().max(Integer::compareTo);
        Optional<Integer> min = numberList.stream().min(Integer::compareTo);
        log.info("max:{},min:{}", max.get(), min.get());

        Queue<String> rewards = new ArrayDeque<>();
        rewards.add("iphone");
        rewards.add("x100");
        rewards.add("Honda Accord");
        rewards.add("Toyota Camry");
        rewards.add("Gopro");

        Runnable task1 = () -> {
          while(rewards.size() > 0){
              Integer guessNumber = Double.valueOf(Math.random() * 100).intValue();
              if(isGoodGuest(guessNumber)){
                  log.info("good guess:{}, reward:{}", guessNumber, rewards.remove());
              }
              try {
                  Thread.sleep(10);
              } catch (InterruptedException e) {
                  throw new RuntimeException(e);
              }
          }
        };

        Runnable task2 = () -> {
            while(rewards.size() > 0) {
                log.info("reward balance={}", rewards.size());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        Thread g1 = new Thread(task1);
        Thread g2 = new Thread(task1);
        Thread g3 = new Thread(task1);
        Thread g4 = new Thread(task1);
        Thread g5 = new Thread(task1);

        Thread report = new Thread(task2);

        g1.start();
        g2.start();
        g3.start();
        g4.start();
        g5.start();

        report.start();

    }

}
