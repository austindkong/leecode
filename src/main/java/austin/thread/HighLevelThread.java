package austin.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @Description:
 * @Author: Austin
 * @Create: 1/31/2024 3:54 PM
 */
@Slf4j
public class HighLevelThread {

    public static void main(String[] args) {


        ExecutorService executor = Executors.newFixedThreadPool(5);

        Callable<Long> task;
        task = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                Integer start = Double.valueOf(Math.random() * 2000).intValue();
                log.info("start={}", start);
                Long result = 0L;
                for(int i=0; i<start; i++){
                    result += i;
                }
                return result;
            }
        };

        Future<Long> future = executor.submit(task);
        while(!future.isDone()){
            log.info("wait");
        }
        try {
            log.info("result:{}", future.get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
        executor.shutdownNow();

    }

}
