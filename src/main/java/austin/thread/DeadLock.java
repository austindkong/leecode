package austin.thread;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * @Description:
 * @Author: Austin
 * @Create: 2/21/2024 9:41 PM
 */
@Slf4j
public class DeadLock {

    public static void main(String[] args) {
        Object lockA = new Object();
        Object lockB = new Object();

        LocalDateTime start = LocalDateTime.now();

        Runnable r1 = () -> {
            synchronized (lockA){
                log.info("I get lock A");
                try {
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            synchronized (lockB){
                log.info("I get lock B");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

        };
        Runnable r2 = () -> {
            synchronized (lockB){
                log.info("I get lock B");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            synchronized (lockA){
                log.info("I get lock A");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        t1.setName("t1");
        t2.setName("t2");
        t1.start();
        t2.start();

    }

}
