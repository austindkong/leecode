package austin.thread;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Description:
 * @Author: Austin
 * @Create: 2/23/2024 2:20 PM
 */
@Slf4j
public class ProducerConsumerHighLevel {

    private static final int MAX_CONSUME = 500;

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Instant start = Instant.now();
        ProducerConsumerHighLevel pc = new ProducerConsumerHighLevel();
        Shared shared = pc.new Shared();

        startNewThread(pc.new MonitorTask(shared), "monitor");

        Lock lock = new ReentrantLock(true);
        Condition condition = lock.newCondition();

        startNewThread(pc.new ProducerTask(shared, lock, condition), "producer");

        ExecutorService consumers = Executors.newFixedThreadPool(10);
        List<Future<?>> futureList = new ArrayList<>();
        for(int i=0; i < 10; i++){
            Future<?> future = consumers.submit(pc.new ConsumerTask(shared, lock, condition));
            futureList.add(future);
        }
        for(Future<?> f : futureList){
            f.get();
        }
        consumers.shutdownNow();

        Duration duration = Duration.between(start, Instant.now());
        log.info("time used:{}, speed:{} per second", duration.toSeconds(), Double.valueOf(MAX_CONSUME/duration.getSeconds()));
    }

    private static void startNewThread(Runnable runnable, String threadName) {
        Thread t = new Thread(runnable);
        t.setName(threadName);
        t.start();
    }

    class Shared {
        Queue<String> orderQueue = new LinkedBlockingQueue<>();
        AtomicLong consumedNumber = new AtomicLong();
        AtomicLong producedNumber = new AtomicLong();
        AtomicBoolean isShutdown = new AtomicBoolean();

        AtomicBoolean isWriteable = new AtomicBoolean();

        public void addOrder(String orderId){
            orderQueue.add(orderId);
            increaseProducedNumber();
            return;
        }

        public void addManyOrder(){
            List<String> orderList = new ArrayList<>();
            for(int i=0; i<5; i++) {
                String orderId = RandomStringUtils.random(20, true, true);
                orderList.add(orderId);
            }

            orderList.stream().forEach(v -> {
                orderQueue.add(v);
                increaseProducedNumber();
            });
            return;
        }

        public String getOrder(){
            if(!isWriteable()) {
                setIsWriteable(true);
                return orderQueue.poll();
            }

            return null;
        }

        public long getQueueSize(){
            return orderQueue.size();
        }

        public boolean isWriteable(){
            return isWriteable.get();
        }

        public void setIsWriteable(boolean res){
            isWriteable.set(res);
        }

        public void shutdown(){
            isShutdown.set(true);
        }

        public boolean isShutdown(){
            return isShutdown.get();
        }

        public long increaseProducedNumber(){
            return producedNumber.incrementAndGet();
        }

        public long increaseConsumedNumber(){
            return consumedNumber.incrementAndGet();
        }

        public long getConsumedNumber(){
            return consumedNumber.get();
        }

        public long getProducedNumber(){
            return producedNumber.get();
        }

    }

    class ProducerTask implements Runnable{

        private Shared shared;

        private final Lock lock;

        private final Condition condition;

        ProducerTask(Shared s, Lock l, Condition c){
            shared = s;
            lock = l;
            condition = c;
        }

        @Override
        public void run() {
            while (!shared.isShutdown()) {
                lock.lock();
                if(shared.isWriteable()) {
                    String orderId = RandomStringUtils.random(20, true, true);
                    shared.addOrder(orderId);
                    log.info("new order is added : {}", orderId);
                    shared.setIsWriteable(false);
                    condition.signalAll();
                    lock.unlock();
                    continue;
                }
                try {
                    log.info("producer is waiting");
                    condition.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } finally {
                    lock.unlock();
                }
            }
            log.info("producer thread is closing");
        }

    }


    class ConsumerTask implements Runnable{

        private static final String URL = "jdbc:postgresql://localhost:5432/test";
        private static final String USER = "postgres";
        private static final String PASSWORD = "531135";

        private final Lock lock;

        private final Condition condition;

        private Shared shared;

        ConsumerTask(Shared s, Lock l, Condition c){
            shared = s;
            lock = l;
            condition = c;
        }

        @Override
        public void run() {
            while (!shared.isShutdown()) {
                if(shared.getConsumedNumber() >= MAX_CONSUME){

                    shared.shutdown();
                    break;
                }
                lock.lock();
                if(!shared.isWriteable()) {
                    String orderId = shared.getOrder();
                    String threadName = Thread.currentThread().getName();
                    if (!StringUtils.isEmpty(orderId)) {
                        saveDb(orderId, threadName, LocalDateTime.now());
                        log.info("{} -> order {} is consumed.", shared.increaseConsumedNumber(), orderId);
                    }else {
                        log.warn("fetched orderId is null");
                    }
                    shared.setIsWriteable(true);
                    condition.signalAll(); // if using signal(), the program might not be closed when max_consume limit is reached.
                    lock.unlock();
                    continue;
                }
                try {
                    log.info("consumer is waiting");
                    condition.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } finally {
                    lock.unlock();
                }
            }
            log.info("consumer thread is closing");
        }

        private void saveDb(String orderId, String note, LocalDateTime ct) {
            // JDBC variables for opening, closing, and managing the connection
            try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
                // The SQL query to insert data
                String insertQuery = "INSERT INTO hotel_order (order_id, note, created_at) VALUES (?, ?, ?)";

                try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery)) {
                    // Set values for the placeholders in the query
                    preparedStatement.setString(1, orderId);
                    preparedStatement.setString(2, note);
                    preparedStatement.setTimestamp(3, Timestamp.valueOf(ct));

                    // Execute the query
                    int rowsAffected = preparedStatement.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    class MonitorTask implements Runnable{

        private Shared shared;

        MonitorTask(Shared s){
            shared = s;
        }

        @Override
        public void run(){
            while (!shared.isShutdown()) {
                log.info("size of queue:{}", shared.getQueueSize());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            log.info("monitor is going to shutdown, producedNumber:{}, queue size:{}, consumedNumber:{}", shared.getProducedNumber(),
                    shared.getQueueSize(), shared.getConsumedNumber());
        }
    }

}
