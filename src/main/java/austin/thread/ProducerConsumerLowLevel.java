package austin.thread;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @Description:
 * @Author: Austin
 * @Create: 2/23/2024 2:20 PM
 */
@Slf4j
public class ProducerConsumerLowLevel {

    private static final int MAX_CONSUME = 500;

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Instant start = Instant.now();

        ProducerConsumerLowLevel pc = new ProducerConsumerLowLevel();
        Shared shared = pc.new Shared();

        startNewThread(pc.new MonitorTask(shared), "monitor");
        startNewThread(pc.new ProducerTask(shared), "producer");

        for(int i=0; i < 10; i++){
            StringBuilder stringBuilder = new StringBuilder("consumer-");
            startNewThread(pc.new ConsumerTask(shared), stringBuilder.append(i).toString());
        }

        Duration duration = Duration.between(start, Instant.now());
        log.info("time used:{}, speed:{} per second", duration.toSeconds(), Double.valueOf(MAX_CONSUME/duration.getSeconds()));
    }

    private static void startNewThread(Runnable runnable, String threadName) {
        Thread t = new Thread(runnable);
        t.setName(threadName);
        t.start();
    }

    class Shared {
        Queue<String> orderQueue = new LinkedBlockingQueue<>();
        AtomicLong consumedNumber = new AtomicLong();
        AtomicLong producedNumber = new AtomicLong();
        AtomicBoolean isShutdown = new AtomicBoolean();

        AtomicBoolean isWriteable = new AtomicBoolean();

        public synchronized void addOrder(String orderId){
            if(isWriteable()) {
                orderQueue.add(orderId);
                setIsWriteable(false);
                notify();
                return;
            }
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        public synchronized void addManyOrder(){
            List<String> orderList = new ArrayList<>();
            for(int i=0; i<5; i++) {
                String orderId = RandomStringUtils.random(20, true, true);
                orderList.add(orderId);
            }

            if(isWriteable()) {
                orderList.stream().forEach(v -> {
                    orderQueue.add(v);
                    increaseProducedNumber();
                    log.info("new order {} has been added", v);
                });
                setIsWriteable(false);
                notifyAll();
                return;
            }
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }


        public synchronized String getOrder(){
            if(!isWriteable()) {
                setIsWriteable(true);
                notify();
                return orderQueue.poll();
            }
            try {
                wait();
                return null;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        public long getQueueSize(){
            return orderQueue.size();
        }

        public boolean isWriteable(){
            return isWriteable.get();
        }

        public void setIsWriteable(boolean res){
            isWriteable.set(res);
        }

        public void shutdown(){
            isShutdown.set(true);
        }

        public boolean isShutdown(){
            return isShutdown.get();
        }

        public long increaseProducedNumber(){
            return producedNumber.incrementAndGet();
        }

        public long increaseConsumedNumber(){
            return consumedNumber.incrementAndGet();
        }

        public long getConsumedNumber(){
            return consumedNumber.get();
        }

        public long getProducedNumber(){
            return producedNumber.get();
        }

    }

    class ProducerTask implements Runnable{

        private Shared shared;

        ProducerTask(Shared s){
            shared = s;
        }

        @Override
        public void run() {
            while (!shared.isShutdown()) {
                synchronized (shared) {
                    shared.addManyOrder();
                }
            }
        }

    }


    class ConsumerTask implements Runnable{

        private static final String URL = "jdbc:postgresql://localhost:5432/test";
        private static final String USER = "postgres";
        private static final String PASSWORD = "531135";


        private Shared shared;

        ConsumerTask(Shared s){
            shared = s;
        }

        @Override
        public void run() {
            while (!shared.isShutdown()) {
                synchronized (shared) {
                    if(shared.getConsumedNumber() >= MAX_CONSUME){
                        shared.shutdown();
                        break;
                    }
                    String orderId = shared.getOrder();
                    if (!StringUtils.isEmpty(orderId)) {
                        String threadName = Thread.currentThread().getName();
                        saveDb(orderId, threadName, LocalDateTime.now());
                        log.info("{} -> order {} is consumed.", shared.increaseConsumedNumber(), orderId);
                    }
                }
            }
        }

        private void saveDb(String orderId, String note, LocalDateTime ct) {
            // JDBC variables for opening, closing, and managing the connection
            try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
                // The SQL query to insert data
                String insertQuery = "INSERT INTO hotel_order (order_id, note, created_at) VALUES (?, ?, ?)";

                try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery)) {
                    // Set values for the placeholders in the query
                    preparedStatement.setString(1, orderId);
                    preparedStatement.setString(2, note);
                    preparedStatement.setTimestamp(3, Timestamp.valueOf(ct));

                    // Execute the query
                    int rowsAffected = preparedStatement.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    class MonitorTask implements Runnable{

        private Shared shared;

        MonitorTask(Shared s){
            shared = s;
        }

        @Override
        public void run(){
            while (!shared.isShutdown()) {
                log.info("size of queue:{}", shared.getQueueSize());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            log.info("monitor is going to shutdown, producedNumber:{}, queue size:{}, consumedNumber:{}", shared.getProducedNumber(),
                    shared.getQueueSize(), shared.getConsumedNumber());
        }
    }

}
