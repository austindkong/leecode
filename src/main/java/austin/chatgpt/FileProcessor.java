package austin.chatgpt;

/**
 * @Description:
 * @Author: Austin
 * @Create: 12/26/2023 4:10 PM
 */


import java.io.File;

public class FileProcessor {

    public static void main(String[] args) {
        String folderPath = "/path/to/your/folder"; // Replace with the actual folder path

        // Step 1: Read a specific folder
        File folder = new File(folderPath);

        if (folder.exists() && folder.isDirectory()) {
            // Step 2: Filter files with ".csv" suffix and print name and size
            File[] files = folder.listFiles((dir, name) -> name.endsWith(".csv"));

            if (files != null) {
                for (File file : files) {
                    System.out.println("File Name: " + file.getName());
                    System.out.println("File Size (bytes): " + file.length());
                    System.out.println("---------------");
                }
            }

            // Step 3: Delete files not ending with ".csv"
            deleteNonCSVFiles(folder);
        } else {
            System.out.println("Invalid folder path");
        }
    }

    private static void deleteNonCSVFiles(File folder) {
        File[] files = folder.listFiles();

        if (files != null) {
            for (File file : files) {
                if (!file.getName().endsWith(".csv")) {
                    if (file.delete()) {
                        System.out.println("Deleted: " + file.getName());
                    } else {
                        System.out.println("Failed to delete: " + file.getName());
                    }
                }
            }
        }
    }
}



