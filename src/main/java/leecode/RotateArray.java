package leecode;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import java.time.Duration;
import java.time.Instant;

/**
 * @Description:
 * @Author: Austin
 * @Create: 2/22/2024 8:21 AM
 */
@Slf4j
public class RotateArray {

    private final static int MAX = 10 * 10000;

    public static void main(String[] args) {
        int[] input = generateInput(61000);
        int step = 59999;
        //log.info("original->{}, total steps:{}", ArrayUtils.toString(input), step);
        if(step > input.length){
            step = step % input.length;
        }
        log.info("new step:{}", step);
        Instant start = Instant.now();
        //timeLimitExceeded(input, step);
        roate(input, step);
        log.info("time spent:{}", Duration.between(start, Instant.now()).toMillis());
    }

    private static void roate(int[] input, int step) {
        int len = input.length;
        if(len < 1 || len > MAX){
            return;
        }
        if(step < 0 || step > MAX){
            return;
        }
        int cnt = 0;
        while(cnt < step){
            int last = input[len-1];
            for(int i=len-1; i>=1; i--){
                input[i] = input[i-1];
            }
            input[0] = last;
            cnt++;
            //log.info("{}->{}", cnt, ArrayUtils.toString(input));
        }
    }

    private static int[] generateInput(int size){
        int[] result = new int[size];
        for(int i=0; i<size; i++){
            result[i] = Double.valueOf(Math.random() * 50000).intValue();
        }
        return result;
    }

    private static void timeLimitExceeded(int[] input, int step){
        int len = input.length;
        if(len < 1 || len > MAX){
            return;
        }
        if(step < 0 || step > MAX){
            return;
        }
        int cnt = 0;
        int[] temp = new int[len];
        while(cnt < step){
            int last = input[len-1];
            for(int i=0; i<len-1; i++){
                temp[i+1] = input[i];
            }
            temp[0] = last;
            cnt++;
            copyArray(temp, input);
            //log.info("{}->{}", cnt, ArrayUtils.toString(input));
        }
    }

    private static void copyArray(int[] from, int[] to){
        if(from.length != to.length){
            return;
        }
        for(int i=0; i<from.length; i++){
            to[i] = from[i];
        }
    }
}
