import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: Austin
 * @Create: 2/28/2024 11:24 AM
 */
@Slf4j
public class Kforce {


    public static void main(String[] args) {
        log.info("{}", solution(84));

        String[] s = new String[]{"abc","AD", "ace"};
        Collections.sort(Arrays.asList(s), String::compareToIgnoreCase);
        log.info("{}", Arrays.asList(s).stream().collect(Collectors.joining(",")));


    }

    static int solution(int n) {
        if(n < 10 || n > 99){
            return 0;
        }
        String s = String.valueOf(Integer.valueOf(n));
        System.out.println(s);
        int a = Integer.valueOf(s.substring(0,1)).intValue();
        System.out.println("a=" + a);
        int b = Integer.valueOf(s.substring(1,2)).intValue();
        return a+b;
    }


}
