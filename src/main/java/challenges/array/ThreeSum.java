package challenges.array;


import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

/**
 * @Description:
 * @Author: Austin
 * @Create: 2022/9/17 21:39
 */
@Slf4j
public class ThreeSum {

    public static void main(String[] args) {
        int[] source = {-12,4,12,-4,3,2,-3,14,-14,3,-12,-7,2,14,-11,3,-6,6,4,-2,-7,8,8,10,1,3,10,-9,8,5,11,3,-6,0,6,12,-13,-11,12,10,-1,-15,-12,-14,6,-15,-3,-14,6,8,-9,6,1,7,1,10,-5,-4,-14,-12,-14,4,-2,-5,-11,-10,-7,14,-6,12,1,8,4,5,1,-13,-3,5,10,10,-1,-13,1,-15,9,-13,2,11,-2,3,6,-9,14,-11,1,11,-6,1,10,3,-10,-4,-12,9,8,-3,12,12,-13,7,7,1,1,-7,-6,-13,-13,11,13,-8};

        ThreeSum app = new ThreeSum();
        Instant start = Instant.now();
        List<List<Integer>> result1 =  app.threeSum(source);
        log.info("{}, time cost:{}ms", result1.size(), Duration.between(start, Instant.now()).toMillis());
        result1.stream().forEach(c -> {
            log.info("{},{},{}", c.get(0), c.get(1), c.get(2));
        });

        start = Instant.now();
        List<List<Integer>> result2 = app.solution2(source);
        log.info("solution2, {}, time cost:{}ms", result2.size(), Duration.between(start, Instant.now()).toMillis());
        result1.stream().forEach(c -> {
            log.info("{},{},{}", c.get(0), c.get(1), c.get(2));
        });
    }

    public List<List<Integer>> threeSum(int[] nums) {
        if(nums == null || nums.length < 3)
            return null;
        int len = nums.length;
        List<List<Integer>> result = new ArrayList<>();
        if(len == 3){
            if((nums[0] + nums[1] + nums[2]) == 0){
                result.add(Arrays.asList(nums[0],nums[1],nums[2]));
                return result;
            }
            return result;
        }
        log.info("length of input:{}", len);
        int cnt = 0;
        for(int i=0; i<len; i++){
            for(int j=i+1; j<len; j++){
                Set<Integer> kValues = new HashSet<>();
                for(int k=j+1; k<len; k++){
                    if(!kValues.contains(nums[k])){
                        kValues.add(nums[k]);
                    }
                }
                //System.out.println("size of kValues:" + kValues.size());
                if(kValues.size() == 0){
                    continue;
                }
                Integer[] kArray = kValues.toArray(new Integer[kValues.size()]);
                for(int m=0; m < kArray.length; m++){
                    cnt++;
                    List<Integer> satisfiedTriplet = Arrays.asList(nums[i],nums[j], kArray[m]);
                    if(nums[i] + nums[j] + kArray[m]==0){
                        if(!containSameElement(satisfiedTriplet, result)){
                            result.add(satisfiedTriplet);
                        }
                    }
                }
            }
        }
        log.info("cnt={}", cnt);

        return result;
    }

    public List<List<Integer>> solution2(int[] nums) {
        Arrays.sort(nums);

        ArrayList<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < nums.length; i++) {
            int j = i + 1;
            int k = nums.length - 1;

            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            while (j < k) {
                if (k < nums.length - 1 && nums[k] == nums[k + 1]) {
                    k--;
                    continue;
                }

                if (nums[i] + nums[j] + nums[k] > 0) {
                    k--;
                } else if (nums[i] + nums[j] + nums[k] < 0) {
                    j++;
                } else {
                    ArrayList<Integer> l = new ArrayList<>();
                    l.add(nums[i]);
                    l.add(nums[j]);
                    l.add(nums[k]);
                    result.add(l);
                    j++;
                    k--;
                }
            }
        }

        return result;
    }

    private boolean containSameElement(List<Integer> element, List<List<Integer>> list){
        boolean hasSame = false;
        for(List<Integer> item : list){
            if(isSameList(item, element)){
                hasSame = true;
                break;
            }
        }
        return hasSame;
    }

    private boolean isSameList(List<Integer> list1, List<Integer> list2){
        Collections.sort(list1);
        Collections.sort(list2);
        return list1.equals(list2);
    }

}
