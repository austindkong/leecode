package challenges.math;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

/**
 * @Description: Exercise 2.2.6
 * @Author: Austin
 * @Create: 1/13/2024 9:27 PM
 */
@Slf4j
public class Checksum {

    public static void main(String[] args) {
        log.info("{}", calcChecksum(87654321));
    }

    static int calcChecksum(Integer number){
        int[] intArray = toIntegerArray(number);

        int sum = 0;
        for(int i=0; i< intArray.length; i++){
            int temp =  (i+1) * intArray[i];
            log.info("temp:{}", temp);
            sum = sum + temp;
        }
        log.info("sum={}", sum);
        return sum%10;
    }

    static int[] toIntegerArray(Integer number){
        String[] strArr = String.valueOf(number).split("");
        int[] result = Arrays.stream(strArr).mapToInt(v -> Integer.valueOf(v)).toArray();
        return result;
    }
}
