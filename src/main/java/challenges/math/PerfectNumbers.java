package challenges.math;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description: Exercise 2.2.3
 * @Author: Austin
 * @Create: 1/13/2024 3:33 PM
 */
@Slf4j
public class PerfectNumbers {

    public static void main(String[] args) {
        checkResult(10000);
    }

    public static void checkResult(Integer number){
        for(int i=1; i< number; i++) {
            List<Integer> result = calcPerfectNumbers(i);
            if (!Objects.isNull(result)) {
                log.info("this is a perfect number: {}, they are: {}", i,
                        result.stream().map(v -> String.valueOf(v)).collect(Collectors.joining(", ")));
            }
        }
    }

    public static List<Integer> calcPerfectNumbers(Integer number){
        //log.info("target number:{}", number);
        List<Integer> divisors = new ArrayList<>();
        for(int i = 1; i< number; i++){
            if(number%i == 0){
                divisors.add(i);
                //log.info("{} is a divisor", i);
            }
        }
        return divisors.stream().mapToInt(Integer::intValue).sum() == number.intValue() ? divisors : null;
    }
}
