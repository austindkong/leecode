package challenges.collection;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description:7.2.5
 * @Author: Austin
 * @Create: 1/13/2024 10:58 PM
 */
@Slf4j
public class LongestGrowingSeq {
    public static void main(String[] args) {
        List<Integer> priceList = Arrays.asList(1,2,3,4,5,6,3,2,3,4);
        List<Integer>  result = findLongestGrowingSequence(priceList);
        log.info("result:{}", result.stream().map(v -> String.valueOf(v)).collect(Collectors.joining(",")));
    }

    private static List<Integer>  findLongestGrowingSequence(List<Integer> priceList) {
        SortedMap<Integer, Integer>  map = new TreeMap<>();
        for(int i=0; i<priceList.size(); i++){
            map.put(getSublist(priceList, i).size(), i);
        }

        log.info("size:{}, lastkey:{}, lastvalue:{}", map.size(), map.lastKey(), map.get(map.lastKey()));
        return priceList.subList(map.get(map.lastKey()), map.get(map.lastKey())+map.lastKey());
    }

    static List<Integer> getSublist(List<Integer> priceList, int pos){
        List<Integer> goodList = new ArrayList<>();
        goodList.add(priceList.get(pos));
        for(int i=pos; i<priceList.size(); i++){
            if(i+1 >= priceList.size())
                break;

            if(priceList.get(i+1) >= priceList.get(i)){
                goodList.add(priceList.get(i+1));
            }else{
                break;
            }
        }
        return goodList;
    }
}
