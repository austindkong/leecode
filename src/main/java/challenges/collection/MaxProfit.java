package challenges.collection;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

/**
 * @Description:  7.2.4
 * @Author: Austin
 * @Create: 1/13/2024 10:33 PM
 */
@Slf4j
public class MaxProfit {

    public static void main(String[] args) {
        List<Integer> priceList = Arrays.asList(20,19,15,14,20);
        calcMaxProfit(priceList);
    }

    private static void calcMaxProfit(List<Integer> priceList) {
        int maxProfit = 0;
        for(int i=0; i<priceList.size(); i++){
            int maxTempProfit = 0;
            for(int j=i+1; j < priceList.size(); j++){
                maxTempProfit = maxTempProfit > (priceList.get(j) - priceList.get(i)) ? maxTempProfit :  (priceList.get(j) - priceList.get(i));
            }
            maxProfit = maxProfit > maxTempProfit ? maxProfit : maxTempProfit;
        }
        log.info("maxProfit:{}", maxProfit);
    }

}
